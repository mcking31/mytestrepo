$(function(){	
	window.dialogloading = $(".dialog-loading");
	
	dialogloading.dialog({
		title : 'Processing...',
		autoOpen: false,
		modal: true,
		resizable: false,
		draggable: false,
		width: 300,
		height: 103,
		show: 'fadeIn',
		hide: 'fadeOut',
		closeOnEscape: false,
		open: function(event, ui) {  $(".ui-dialog-titlebar-close", $(this).parent()).hide(); }
	}).parent().css({position:"fixed"}).end();
	
	window.dialogmessage = $(".dialog-message");
	
	dialogmessage.dialog({
		title : 'System Message',
		autoOpen: false,
		modal: true,
		resizable: false,
		draggable: false,
		width: 300,
		height: 'auto',
		show: 'fadeIn',
		hide: 'fadeOut'
	}).parent().css({position:"fixed"}).end();
	
	window.show_errors_login = function(errors){
		$.each(errors, function( index, value ) {
			$(
				"<span></span>"
			).text(
				value
			).attr(
				'class', 'label label-danger'
			).insertBefore($( "#" + index ).parent().parent()).fadeOut(10000);
			
			$("#" + index).parent().parent().addClass("has-error").delay(10000).queue(function(){
				$(this).removeClass("has-error");
				$(this).dequeue();
			});
		});
	}
	
	window.show_errors = function(errors){
		$.each(errors, function( index, value ){			
			$(
				"<span></span>"
			).text(
				value
			).attr(
				'class', 'label label-danger'
			).insertAfter("#" + index).fadeOut(10000);
			
			$("#" + index).parent().parent().addClass("has-error").delay(10000).queue(function(){
				$(this).removeClass("has-error");
				$(this).dequeue();
			});
		});
	}
	
	$("span .label .label-danger, .suc").fadeOut(10000);
	$( "<div>" ).removeClass( "has-error" );
	
	window.check_session = function( data ){
		if(typeof data.session !== "undefined"){
			if( !data.session ){
				window.location.href = baseurl + "logout";
				window.location.reload();
			}
		}
	}
	
	window.failed_post_process = function( data ){
		if( typeof data.errors !== "undefined" )
			show_errors( data.errors );
				
		if( 
			typeof data.message !== "undefined" 
			&&
			data.message.length > 0
		)
			dialogmessage.html(data.message).dialog("open");
	}
	
	window.failed_post_process_login = function( data ){
		if( typeof data.errors !== "undefined" )
			show_errors_login( data.errors );
				
		if( 
			typeof data.message !== "undefined" 
			&&
			data.message.length > 0
		)
			dialogmessage.html(data.message).dialog("open");
	}
	
});