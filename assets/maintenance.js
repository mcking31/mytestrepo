$(function(){
	var controller = 'maintenance';
	
	$(".create_btn").click(function(){		
		var _form = $(this).attr('alt');
		var data = $('#' + _form + ' :input').serializeArray();
		
		data = data.concat(
			$('#' + _form + ' input[type=checkbox]:not(:checked)').map(function(){
				return {"name": this.name, "value": 0}
            }).get()
		);
		
		dialogloading.dialog('open');
		
		$.post(
			baseurl + controller + "/action",
			data
		).done(function( data ){
			dialogloading.dialog('close');
			
			if( data.response ){
				$('table[alt="' + $('#' + _form + ' input[name="tbl"]').val() + '"]').DataTable().ajax.reload();
				$(".reset_form[alt='" + _form + "']").click();
			} else
				failed_post_process( data );
				
			if( typeof data.message !== "undefined" )
				dialogmessage.html( data.message ).dialog('open');
		});
	});
	
	$(".reset_form").each(function(){
		$(this).click(function(){
			var _form = $(this).attr('alt');
			
			$( "#" + _form + " input" ).each(function(){
				if( $( this ).attr('name') != 'tbl' )
					$( this ).val( '' );
			});
			
			$( "#" + _form + " select" ).each(function(){
				$( this ).val( '' );
			});
			
			$('#' + _form + ' legend[id="action"]').html('<h4>Create</h4>');
			
			$('.create_btn[alt="' + _form + '"]').text('Create');
		});
	});
});

$(document).ready(function(){
	var controller = 'maintenance';
	
	$('table.display').each(function(){
		$this = $(this);
		
		var _fields = [];
		
		$this.find('thead th').each(function(){			
			if( typeof $(this).attr('alt') !== "undefined" )
				_fields.push( { data : $(this).attr('alt') } );
		});
		
		_fields.push(
			{
				data : null,
				render: function( data, type, row ){
					return "<a href='javascript:void(0);' class='btn btn-danger btn-xs btn_delete' alt='"+ data.pkey +"'>Delete</a>&nbsp;<a href='javascript:void(0);' class='btn btn-primary btn-xs btn_update' alt='"+ data.pkey +"'>Update</a>";
				}
			}
		);
		
		$(this).dataTable({
			"processing": true,
			"serverSide": true,
			"stateSave": true,
			"iDisplayLength":10,
			"aoColumnDefs": [
				{ 
					'bSortable': false,
					'aTargets': [ -1 ]
				}
			],
			"ajax": {
				"url": controller + '/get_data',
				"type": 'POST',
				"data": { "tbl" : $this.attr('alt') }
			},
			'columns': _fields,
			'createdRow': function ( row, data, indexe ) {
				$('td a.btn_delete', row).click(function(){
					if( confirm( 'Are you sure that you want to delete this record?' ) ){
						var $tbl = $( this ).parent().parent().parent().parent().attr('alt');
						$.post(
							controller + '/delete',
							{
								tbl : $tbl,
								pkey : $( this ).attr('alt')
							}
						).done(function( data ){
							if( data.response )
								$('table[alt="' + $tbl + '"]').DataTable().ajax.reload();
							
							if( typeof data.message !== "undefined" )
								dialogmessage.html( data.message ).dialog('open');
						});
					}
				});
				$('td a.btn_update', row).click(function(){
					var $tbl = $( this ).parent().parent().parent().parent().attr('alt');
					$.post(
						controller + '/get_data_by_id',
						{
							tbl : $tbl,
							pkey : $( this ).attr('alt')
						}
					).done(function( data ){
						if( data.response ){
							var $form = $('input[value="'+ $tbl +'"]').parent().parent().parent();
							
							$form.find('.create_btn').text('Modify');
							$form.find('legend').html('<h4>Modify</h4>');
							
							$.each(data.details, function( index, value ){
								$.each(value, function( field, val ){
									$('input[name="'+ field +'"]').val( val );
									$('select[name="'+ field +'"]').val( val );
								});
							});
						}
						
						if( typeof data.message !== "undefined" )
							dialogmessage.html( data.message ).dialog('open');
					});
				});
			}
		});
	});
});