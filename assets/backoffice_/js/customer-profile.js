(function($) {
	$(document).ready(function(e) {
        $('#search_btn').click(function(){
			var $btn = $(this);
			
			if( $("input[id='fname']").val() != "" && $("input[id='lname']").val() != "" && $("input[id='walletid']").val() != "" ){
				$.post(
					baseurl + 'customer_profile/search',
					{
						'firstname' : $("input[id='fname']").val(),
						'lastname' : $("input[id='lname']").val(),
						'walletid' : $("input[id='walletid']").val()
					}
				).done(function(data){
					if( data.response ){
						if( data['data'].length > 0 ){
							$(".result").empty();
							$.each(data['data'], function(index, value){
								// <td>Lavigne</td>
								// <td>Avril</td>
								// <td>0917 211 2392</td>
								// <td>09-16-1995</td>
								// <td class="nowrap">P 25,000.00</td>
								// <td class="nowrap">1234-5275-1234</td>
								// <td>08-03-2016</td>
								$(".result").append(
									$(
										'<tr></tr>'
									).append(
										$(
											'<td></td>'
										).html( value['LastName'] )
									).append(
										$(
											'<td></td>'
										).html( value['FirstName'] )
									).append(
										$(
											'<td></td>'
										).html( value['MobileNumber'] )
									).append(
										$(
											'<td></td>'
										).html( value['Birthday'] )
									).append(
										$(
											'<td></td>',
											{
												"class" : "nowrap"
											}
										).html( value['AvailableBalance'] )
									).append(
										$(
											'<td></td>',
											{
												"class" : "nowrap"
											}
										).html( value['WalletId'] )
									).append(
										$(
											'<td></td>'
										).html( value['DateCreated'] )
									)
								);
							});
							$(".no-result").addClass('hidden');
						} else {
							$(".no-result").removeClass('hidden');
						}
					} else {
						
					}
					
					$btn.html('Search').removeClass('disabled');
				});
			} else {
				$btn.html('Search').removeClass('disabled');
			}
		});
	});
})(jQuery);