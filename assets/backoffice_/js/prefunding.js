(function($) {
	$("#prefundSubmit").on("click", function(){
		var companyName = $("select[id='company-name']").val(); // $("#company-name").val();
		var _companyName = $("select[id='company-name']").find("option[value=" + companyName + "]").html();
		
		var amount = $("#amount").val();
		var error = false;

		if(companyName.length === 0){
			error = true;
			$("#company-name").parents(".form").addClass("error");
			$("#company-name").parents(".form").find(".note").text("Please select company name");
			$(".alert-group").html(
                "<div class=\"alert error\">"+
                    "<div class=\"content\">"+
                        "<p class=\"title\">"+
                            "Prefunding Failed"+
                        "</p>"+
                        "<p class=\"description\">"+
                            "Please try again."+
                        "</p>"+
                    "</div>"+
                    "<i class=\"icon fa fa-times close\"></i>"+
                "</div>"
            );
            alertClose();
		}else{
			error = false;
        	$("#company-name").parents(".form").removeClass("error");
			$("#company-name").parents(".form").find(".note").text("");
        	if(amount.length === 0){
        		error = true;
        		$("#amount").parents(".form").addClass("error");
				$("#amount").parents(".form").find(".note").text("Please enter the amount");
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Prefunding Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
	            alertClose();
        	}else if(!$.isNumeric(amount)){
        		error = true;
        		$("#amount").parents(".form").addClass("error");
				$("#amount").parents(".form").find(".note").text("Invalid character input");
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Prefunding Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
	            alertClose();
        	}else{
        		error = false;
        		$(".alert-group").html("");
        	}
		}

		if(error == false){
			$("#mirrorCompany").html( _companyName );
			var modal = $("#prefundSubmit").attr("data-modal");
            $(modal).modal({
                duration: "50",
                closable: false
            }).modal("show");

    		$("#amount").parents(".form").removeClass("error");
			$("#amount").parents(".form").find(".note").text("");
		}
	});

	$("#confirm").on("click", function(){
		var $this = $(this);
		
		$.post(
			baseurl + 'prefunding/prefund',
			{
				'wallet_id' : $("select[id='company-name']").val(),
				'amount' : $("input[id='amount']").val()
			}
		).done(function(data){
			$this.parents(".ui.modal").modal("hide");
			var dataLoadingLabel = $this.attr("data-loading-label");
			$this.removeClass("disabled").html("Confirm");
			
			if( data.response ){
				var cname = $("select[id='company-name']").find("option[value=" + $("select[id='company-name']").val() + "]").html();
				
				$(".alert-group").html(
					"<div class=\"alert success\">"+
						"<div class=\"content\">"+
							"<p class=\"title\">"+
								"Prefunding Completed"+
							"</p>"+
							"<p class=\"description\">"+
								"An amount of <b> P "+ numeral($("#amount").val()).format('0,0.00') + "</b> is credited to <b>"+ cname +"</b> on " + data.transaction_date +
								"<br> An SMS notification will be sent to the customer."+
							"</p>"+
						"</div>"+
						"<i class=\"icon fa fa-times close\"></i>"+
					"</div>"
				);				
			} else {
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Prefunding Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
			}
			$("#amount").val("");
			alertClose();
		});
		
	});

	$("#confirmPrefund").on("click", function(){
		if($(this).is(':checked')){
			$("#confirm").removeClass("disabled").removeAttr("disabled");
		}
		else{
			$("#confirm").addClass("disabled");
		}
	})

	function alertClose(){
		
		$(".alert .close").on("click", function(){
	        $(this).parents(".alert").fadeOut(300, function(){
	            $(this).remove();
	        });
	    });

	    $(".form.error .field").on("change keyup paste", function(){
	        if($(this).val() == ""){
	            $(this).parents(".form").removeClass("error");
	            $(this).parents(".form").find(".note").text("");
	        }
	    });
	}
})(jQuery);