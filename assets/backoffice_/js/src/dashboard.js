(function($) {
    // $('#progress').progress();

    $("#dropMenu").on("click", function(){
        $(this).find(".menu").transition('slide down');
        $(this).toggleClass("active");
    });

    $(".toggle-sidebar").on("click", function(){
        $("body, .wrapper, .sidebar").toggleClass("open");
    });
})(jQuery);