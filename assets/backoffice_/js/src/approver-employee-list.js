(function($) {
    $("#submitReject").on("click", function(){
    	var reason = $("#reason").val();

		if(reason == ""){
        	$(".modal").find(".error-message").html("");
        	$("#reason").parents(".form").addClass("error");
        	$(".ui.modal").modal("refresh");
        }

        if(reason != ""){
        	$(".modal").find(".error-message").html("");
        	$(this).parents(".modal").find(".error-message").text("Please enter the reason for rejection");
        	$("#reason").parents(".form").removeClass("error");
        	$(".ui.modal").modal("refresh");
        }

        if($("#confirmReject").prop('checked') == false){
        	$(".modal").find(".error-message").html("");
            $(this).parents(".modal").find(".error-message").text("Please check the confirmation checkbox");
        	$(".ui.modal").modal("refresh");
        }

        else{
            $(this).parents(".modal").find(".error-message").text("");
            var dataLoadingLabel = $(this).attr("data-loading-label");
            $("#reason").parents(".form").removeClass("error");
        	$(".ui.modal").modal("refresh");
        	$(this).addClass("disabled").html("<div class='ui active tiny inline loader inverted'></div> "+dataLoadingLabel);
        }
    });
})(jQuery);