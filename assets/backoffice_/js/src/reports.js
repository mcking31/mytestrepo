(function($) {

    //Tab functions
    var tabSrc= $(document).find(".tab-header a.active").attr('data-href');
    $("#"+tabSrc).show();

    $(".tab-header a").click(function(){
        if (!$(this).hasClass("active")) {
            var $this = $(this);
            var tabLink = $this.attr('data-href');
            var tabName = $(document).find("#"+tabLink);

            $('.tab-content').animate({opacity:0},100,function(){
                $(".tab-header a").not($this).removeClass("active");
                $this.addClass("active");
                $(".tab-content").not(tabName).hide();
                tabName.show();
                $(this).stop().animate({opacity: "1"},100);
            });
        }
    });

    $(document).ready(function(){
        $('.report-search').keyup(function(){
            searchTables($(this).val(), $(this).attr("data-table"));
        });

    });

    function searchTables(inputVal, dataTable){
        var table = $(dataTable);
        table.find('tbody tr:not(.no-result)').each(function(index, row)
        {
            var allCells = $(row).find('td');
            if(allCells.length > 0)
            {
                var found = false;
                allCells.each(function(index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if(regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });

                if(found == true) {
                    $(row).show()
                }

                else {
                    $(row).hide()
                }

                var noResultRow = table.find('tr.no-result');
                if (table.find('tbody > tr:visible:not(.no-result)').length <= 0) {
                    noResultRow.show();
                }
                else {
                    noResultRow.hide();
                }
            }
        });
    }
})(jQuery);
