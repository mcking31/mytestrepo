(function($) {

    //Setting active class to the label on login
    $(".form.label-set .field, .form.icon .field").focus(function(){
        $(this).parents(".form.label-set").addClass("active");
        $(this).parents(".form.icon").addClass("active");
    }).blur(function(){
        $(this).parents(".form.label-set").removeClass("active");
        $(this).parents(".form.icon").removeClass("active");
    });

    $('th').click(function(){
        var table = $(this).parents('.table').eq(0);
        var rows = table.find('tr:gt(0):not(.no-result)').toArray().sort(comparer($(this).index()))
        if($(this).find(".icon").hasClass("fa-sort")){
            $(this).find(".icon").removeClass("fa-sort").addClass("fa-sort-asc");
        }
        else if($(this).find(".icon").hasClass("fa-sort-desc")){
            $(this).find(".icon").removeClass("fa-sort-desc").addClass("fa-sort-asc");
        }
        else if($(this).find(".icon").hasClass("fa-sort-asc")){
            $(this).find(".icon").removeClass("fa-sort-asc").addClass("fa-sort-desc");
        }
        $(this).parents(".table").find("th").not($(this)).find(".icon").removeClass("fa-sort-asc").addClass("fa-sort");
        $(this).parents(".table").find("th").not($(this)).find(".icon").removeClass("fa-sort-desc").addClass("fa-sort");
        
        this.asc = !this.asc;
        if (!this.asc){
            rows = rows.reverse();
        }
        for (var i = 0; i < rows.length; i++){table.append(rows[i])}
    });


    $(".alert .close").on("click", function(){
        $(this).parents(".alert").fadeOut(300, function(){
            $(this).remove();
        });
    });

    $(document).ready(function(){
        $('#search').keyup(function(){
            searchTable($(this).val());
        });
    });

    var checkboxes = $(".table tbody input[type='checkbox']");

    $(".select-all").change(function(){
        $(this).parents("table").find("tbody input[type='checkbox']").prop('checked', $(this).prop("checked"));
        countCheckbox();
        colSumEmployees();
        colSumDisbursement()
    });

    checkboxes.change(function(){
        countCheckbox();
        colSumEmployees();
        colSumDisbursement()
    });

    $(".loading-after").on("click", function(){
        var dataLoadingLabel = $(this).attr("data-loading-label");
        $(this).addClass("disabled").html("<div class='ui active tiny inline loader inverted'></div> "+dataLoadingLabel);
    });

    $(".single.dropdown").dropdown();

    $(".field.ui.dropdown").dropdown({
        onShow: function(){
            $(this).parents(".form.label-set").addClass("active");
        },
        onHide: function(){
            $(this).parents(".form.label-set").removeClass("active");
        }
    });

    $(".modal-trigger").on("click", function(){
        var modal = $(this).attr("data-modal");
        $(modal).modal({
            duration: "50",
            closable: false
        }).modal("show");
    });

    $(".modal-trigger-coupled").on("click", function(){
        var modal = $(this).attr("data-modal");
        $(modal).modal({
            duration: "50",
            closable: false,
            allowMultiple: false,
            transition: "fade left"
        }).modal("show");
    });

    $(".ui.modal .close").on("click", function(){
        $(this).parents(".modal").modal("hide");
    });

    $(".dimmer-trigger").on("click", function(){
        var dimmer = $(this).attr("data-dimmer");
        $(dimmer).dimmer({
            duration: "50",
            closable: false,
            transition: "fade left"
        }).dimmer("show");
    });

    $(".ui.dimmer .close").on("click", function(){
        $(this).parents(".dimmer").dimmer("hide");
    });

    $(".accordion-head").click(function(){
        var $this = $(this);
        var accordionBody = $this.parents(".accordion-item").find(".accordion-body");
        $(".accordion-head").not($this).removeClass("toggled");
        $(".accordion-head").not($this).find(".icon").removeClass("fa-angle-up");
        $(".accordion-body").not(accordionBody).slideUp("fast");
        $this.toggleClass("toggled");
        $this.find(".icon").toggleClass("fa-angle-up");
        accordionBody.slideToggle("fast");
    });

    $("[autofocus]").on("focus", function() {
        if (this.setSelectionRange) {
            var len = this.value.length * 2;
            this.setSelectionRange(len, len);
        } else {
            this.value = this.value;
        }
    }).focus();

    $("[data-mirror]").on("change", function(){
        var mirrorId = $(this).attr("data-mirror");
        var mirrorValue = $(this).val();
        $(mirrorId).text(mirrorValue);
    });

    $(".mirror-input-amount").on("change", function(){
        var mirrorId = $(this).attr("data-mirror");
        var mirrorValue = $(this).val();
        $(mirrorId).text(numeral(mirrorValue).format('0,0.00'));
    });

    

    function countCheckbox(){
        var countCheckedCheckboxes = checkboxes.filter(':checked').length;
        $('.checked-counter').text(countCheckedCheckboxes);
    }

    function comparer(index) {
        return function(a, b) {
            var valA = getCellValue(a, index), valB = getCellValue(b, index)
            return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
        }
    }

    function getCellValue(row, index){
        return $(row).children('td').eq(index).html()
    }

    function searchTable(inputVal){
        var table = $('.table-sort');
        table.find('tbody tr:not(.no-result)').each(function(index, row)
        {
            var allCells = $(row).find('td');
            if(allCells.length > 0)
            {
                var found = false;
                allCells.each(function(index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if(regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });

                if(found == true) {
                    $(row).show()
                }

                else {
                    $(row).hide()
                }

                var noResultRow = table.find('tr.no-result');
                if (table.find('tbody > tr:visible:not(.no-result)').length <= 0) {
                    noResultRow.show();
                }
                else {
                    noResultRow.hide();
                }
            }
        });
    }

    function colSumEmployees() {
        var sumEmployee=0;
        $('td.employee-count').each(function() {
            var checkboxAttr = $(this).parents("tr").find("input[type='checkbox']");
            if(checkboxAttr.is(":checked") == true){
                sumEmployee += parseInt($(this).text());    
            }
        });

        $('.employee-sum').text(sumEmployee);
    }

    function colSumDisbursement() {
        var sumDisbursement=0;
        $('td .total-salary').each(function() {
            var checkboxAttrDis = $(this).parents("tr").find("input[type='checkbox']");
            if(checkboxAttrDis.is(":checked") == true){
                valTotalSalary = $(this).text();
                sumDisbursement += parseFloat(valTotalSalary.replace(/,/g, ''));    
            }
        });

        $('.total-disbursement').text(numeral(sumDisbursement).format('0,0.00'));
    }

    
})(jQuery);
