(function($) {
	$(document).ready(function(e) {
		// function parseDMY(value) {
			// var date = value.split("-");
			// var y = parseInt(date[0], 10),
				// m = parseInt(date[1], 10),
				// d = parseInt(date[2], 10);
			// return dateFormat(new Date(y, m - 1, d), "m-d-yyyy");
		// }
		
		function parse( value ){
			var today = new Date(value).toLocaleDateString('en-GB', {  
				day : 'numeric',
				month : 'short',
				year : 'numeric'
			}).split(' ').join('-');
		}

        $('#search_btn').click(function(){
			$(".result").empty();
			$('.alert-group').empty();
			var $btn = $(this);
			
			// if( $("input[id='fname']").val() != "" && $("input[id='lname']").val() != "" && $("input[id='walletid']").val() != "" ){
				$.post(
					baseurl + 'customer_profile/search',
					{
						'firstname' : $("input[id='fname']").val(),
						'lastname' : $("input[id='lname']").val(),
						'walletid' : $("input[id='walletid']").val()
					}
				).done(function(data){
					if( data.response ){
						if( data['data'].length > 0 ){
							$(".result").empty();
							$.each(data['data'], function(index, value){
								// <td>Lavigne</td>
								// <td>Avril</td>
								// <td>0917 211 2392</td>
								// <td>09-16-1995</td>
								// <td class="nowrap">P 25,000.00</td>
								// <td class="nowrap">1234-5275-1234</td>
								// <td>08-03-2016</td>
								$(".result").append(
									$(
										'<tr></tr>'
									).append(
										$(
											'<td></td>'
										).html( value['Lastname'] )
									).append(
										$(
											'<td></td>'
										).html( value['FirstName'] )
									).append(
										$(
											'<td></td>'
										).html( value['MobileNumber'] )
									).append(
										$(
											'<td></td>'
										).html( value['BirthDate'].replace("T00:00:00", "") )
									).append(
										$(
											'<td></td>',
											{
												"class" : "nowrap"
											}
										).html( value['CurrentBalance'] )
									).append(
										$(
											'<td></td>',
											{
												"class" : "nowrap"
											}
										).html( value['WalletId'] )
									).append(
										$(
											'<td></td>'
										).html( value['DateCreated'] )
									)
								);
							});
							$(".no-result").addClass('hidden');
						} else {
							$(".no-result").removeClass('hidden');
						}
					} else {
						$(".alert-group").append(
							$(
								'<div></div>',
								{
									'class':'alert error'
								}
							).append(
								$(
									'<div></div>',
									{
										'class':'content'
									}
								).append(
									$(
										'<p></p>',
										{
											'class':'title'
										}
									).html( data.message )
								).append(
									$(
										'<p></p>',
										{
											'class':'description'
										}
									).html( 'Please try again' )
								)
							).append(
								$(
									'<i></i>',
									{
										'class':'icon fa fa-times close'
									}
								).click(function(){
									$('.alert-group').empty();
								})
							)
						);
					}
					
					$btn.html('Search').removeClass('disabled');
				});
			// } else {
				
				// $(".alert-group").append(
					// $(
						// '<div></div>',
						// {
							// 'class':'alert error'
						// }
					// ).append(
						// $(
							// '<div></div>',
							// {
								// 'class':'content'
							// }
						// ).append(
							// $(
								// '<p></p>',
								// {
									// 'class':'title'
								// }
							// ).html( 'Search Profile' )
						// ).append(
							// $(
								// '<p></p>',
								// {
									// 'class':'description'
								// }
							// ).html( 'Invalid search parameters' )
						// )
					// ).append(
						// $(
							// '<i></i>',
							// {
								// 'class':'icon fa fa-times close'
							// }
						// ).click(function(){
							// $('.alert-group').empty();
						// })
					// )
				// );
				// $btn.html('Search').removeClass('disabled');
			// }
		});
	});
})(jQuery);