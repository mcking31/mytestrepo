<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hooks // extends CI_Controller 
{
	public $allowedwosession;
	public $allowedmenus;
	public $url;
	public $allowed;
	
	function __construct()
	{
		parent::__construct();	
		
		$this->allowedwosession = array( 'login', 'maintenance', 'truemoney', 'privacy', 'logout' );
	}
	
	function session_check(){
		$ci =& get_instance();
		$this->url = str_replace( "/", "", $ci->uri->segment(1) );
		
		$this->load->library('common');
		$sess = $this->common->get_session('adminUsersId');
		
		if( !$this->is_allowed() ){
			if( 
				strlen( $sess ) > 0
				&&
				!empty( $sess )
			)
				$this->show_dashboard();
			else
				$this->logout_user();
			
		} else {
			
			if( 
				empty( $sess )
				&&
				!in_array( $this->url, $this->allowedwosession )
			)
				$this->logout_user();
			else {
				if( !in_array( $this->url, array_merge( $this->allowedwosession, $this->allowedmenus ) ) )
					$this->show_dashboard();
				
				if( 
					$this->url == "login"  
					&&
					!empty( $sess )
					&&
					empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'
				)
					redirect( base_url( 'dashboard' ) );
				
				if(
					in_array( $this->url, $this->allowedmenus )
					&&
					!$sess 
					&&
					empty( $sess )
				) 
					$this->logout_user();
					
			}
		}
	}
	
	public function is_allowed(){
		$this->allowedmenus = $this->fetch_user_access();
		
		foreach( $this->allowedmenus as $key => $values ){
			$a = explode( "/", $values );
			$this->allowedmenus[$key] = $a[0];
		}
		
		$allowed = $this->allowed = array_merge( (array)$this->allowedwosession, (array)$this->allowedmenus );
		
		return in_array( $this->url, $allowed );
	}
	
	public function fetch_user_access(){
		$this->load->library('query');
		
		$group_id = $this->common->get_session('group_id');
		
		$res = $this->query->select(
			array(
				'table' => 'cms_menus',
				'joins' => array(
					'user_group_menus' => array(
						'cms_menus.menu_id' => 'user_group_menus.menu_id'
					)
				),
				'conditions' => array(
					'group_id' => $group_id
				)
			)
		);
		
		$_res = array();
		
		foreach( $res as $key => $values ){
			array_push( $_res, $values['url'] );
		}
		
		return $_res;
	}
	
	public function logout_user(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

			$return = array( 'session' => FALSE );
				
			header( 'Content-Type: application/x-json' );
			echo json_encode($return);
			
			die();
			
		} else {
			if( $this->url != "logout" )
				redirect( site_url( 'logout' ) );
		}
	}
	
	public function show_dashboard(){
		if( $this->url != "dashboard" ){
			redirect( base_url( 'dashboard' ) );
		}
	}
}