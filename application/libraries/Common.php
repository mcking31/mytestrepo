<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Common {
	protected $ci;
		
	public function __construct(){
		$this->ci =& get_instance();
    }
	
	public function get_session( $key = '', $print = false ){
		$sess = $this->ci->session->all_userdata();
		
		if( $print )
			debug( $sess );
		
		return ($key != '') ? ( isset( $sess[$key] ) ? $sess[$key] : '' ) : $sess;
	}
	
	/*
	* UTILITIES
	*/
	public function format_arr( $arr = array(), $indention = '---', $res = array(), $id = '', $text = '' ){
		if( 
			is_array( $arr )
		){
			foreach( $arr as $key => $values ){ 
				$res[ $values[ $id ] ] = $indention . "&nbsp;" . $values[ $text ];
				
				if( 
					isset( $values['children'] )
					&&
					is_array( $values['children'] )
				)
					$res = $this->format_arr( $values['children'], $indention . '---', $res, $id, $text );
			}
		}
		
		return $res;
	}
	/*
	* END UTILITIES
	*/
	
}