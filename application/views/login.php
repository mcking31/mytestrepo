<body class="login-page">
    <div class="login">
        <div class="segment">
            <div class="img-holder logo">
                <img src="<?php echo base_url('../assets/images/logo-truemoney-login-pmp.png'); ?>" alt="TrueMoney" class="img-full-width">
            </div>
			<?php // echo form_open('login/verify'); ?>
            <div class="login-form">
                <div class="error-message center-align">
                </div>
                <div class="form label-set">
                    <label for="email">Email / Username</label>
                    <input type="text" class="field" placeholder="Enter email address or username" id="email" autocomplete="off"/>
                </div>
                <div class="form label-set password-form">
                    <label for="password">Password</label>
                    <input type="password" class="field" placeholder="Enter password" id="password"/>
                    <span class="show-password" id="togglePassword" title="Show Password">Show</span>
                </div>
                <div class="form"><input type="hidden" class="field" id="attempts" value="0"></div>
                <div class="form mrg-top-20">
                    <button class="button orange block" data-loading-label="Logging In" id="login">Login</button>
                </div>
            </div>
			<?php // echo form_close(); ?>
        </div>
    </div>
</body>