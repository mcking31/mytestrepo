<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="True Money">
    <meta name="author" content="True Money">
	
	<?php echo $this->template->meta; ?>

    <title><?php echo ( strlen( $this->template->title ) > 0 ) ? $this->template->title . ' | ' : ""; ?> True Money</title>
	
	<?php echo $this->template->stylesheet; ?>
	
	<script type="text/javascript">
		var baseurl     = '<?php echo site_url(); ?>';
	</script>
</head>
<?php 
	echo $this->template->content;
?>
<?php echo $this->template->javascript; ?>
</html>