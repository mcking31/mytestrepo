<div class="alert-group">
	<div class="alert error">
		<div class="content">
			<p class="title">
				No reports found
			</p>
			<p class="description">
				Please try again.
			</p>
		</div>
		<i class="icon fa fa-times close"></i>
	</div>
</div>
<div class="breadcrumb">
	<div class="breadcrumb-group">
		<div class="active section">Reports</div>
	</div>
</div>
<div class="box mrg-bt-30">
	<h3 class="box-title">
		Search Report
	</h3>
	<div class="mx-wd-800">
		<div class="row group input-daterange form-group">
			<div class="col span5 me">
				<div class="form label-set icon">
					<label for="startDate">Start Date</label>
					<input type="text" class="field daterange" id="startDate" autocomplete="off" placeholder="Select start date here" readonly />
					<i class="icon fa fa-angle-down date-dropdown"></i>
				</div>
				<div id="startDatepicker" class="datepicker-wrapper">
					
				</div>
			</div>
			<div class="col span5 me">
				<div class="form label-set icon">
					<label for="endDate">End Date</label>
					<input type="text" class="field daterange" id="endDate" autocomplete="off" placeholder="Select start date here" readonly />
					<i class="icon fa fa-angle-down date-dropdown"></i>
				</div>
				<div id="endDatepicker" class="datepicker-wrapper">
					
				</div>
			</div>
			<div class="col span2">
				<div class="form mrg-top-10">
					<button class="button orange fat loading-after" data-loading-label="Searching">View</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="header mrg-bt-20">
	<div class="row group">
		<div class="col span8">
			<h3>
				Search Results
			</h3>
		</div>
		<div class="col span4">
			<div class="form search icon">
				<input type="text" class="field" placeholder="Search" id="search"/>
				<i class="icon fa icon-search"></i>
			</div>
		</div>
	</div>
</div>
<div class="table-group">
	<table class="table table-sort">
		<thead>
			<tr>
				<th width="10%" class="nowrap">Transaction ID <span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">Transaction Type <span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">Transaction Date <span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">Profile ID<span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">Wallet ID<span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">Card Number<span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">Last Name<span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">First Name<span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">Channel<span class="icon fa fa-sort"></span></th>
				<th width="10%" class="nowrap">Amount<span class="icon fa fa-sort"></span></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1234423</td>
				<td>Pera Out</td>
				<td>09-12-2016</td>
				<td class="nowrap">1234-2344-3244</td>
				<td class="nowrap">3232-6544-2344</td>
				<td class="nowrap">8678-1231-2343</td>
				<td class="nowrap">Moore</td>
				<td class="nowrap">Roger</td>
				<td>ExpressPay</td>
				<td class="nowrap">P 50,000.00</td>
			</tr>
		</tbody>
		<tfoot>
			<tr class="no-result center-align hidden">
				<td colspan="7">No results found.</td>
			</tr>
		</tfoot>
		<!-- <tfoot>
			<tr>
				<td colspan="5">
					<div class="empty-data">
						<div class="mrg-bt-20"><i class="icon fa icon-employees"></i></div>
						<div>
							<p class="bold">
								Looks like you haven't uploaded an Employee Registration form yet
							</p>
							<p>
								Details will be available once you upload an Employee Registration form. <a href="#" class="bold orange-color">Learn More</a>
							</p>
						</div>
					</div>
				</td>
			</tr>
		</tfoot> -->
	</table>
</div>