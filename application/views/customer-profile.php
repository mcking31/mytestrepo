<div class="alert-group">
	
</div>
<div class="breadcrumb">
	<div class="breadcrumb-group">
		<div class="active section">Customer Profile</div>
	</div>
</div>
<div class="box mrg-bt-20">
	<div class="box-title">
		<h3>
			Search Profile
		</h3>
	</div>
	<div class="box-content mrg-top-30">
		<div class="mx-wd-1000">
			<div class="row group form-group">
				<div class="col span10">
					<div class="row group">
						<div class="col span4">
							<div class="form">
								<div class="form label-set">
									<label for="fname">Customer's First Name</label>
									<input type="text" class="field" id="fname" placeholder="Enter the first name">
								</div>
							</div>
						</div>
						<div class="col span4">
							<div class="form">
								<div class="form label-set">
									<label for="lname">Customer's Last Name</label>
									<input type="text" class="field" id="lname" placeholder="Enter the last name">
								</div>
							</div>
						</div>
						<div class="col span4">
							<div class="form">
								<div class="form label-set">
									<label for="walletid">Wallet ID</label>
									<input type="text" class="field" id="walletid" placeholder="Enter the wallet ID">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col span2">
					<div class="form mrg-top-10">
						<button class="button orange block loading-after" data-loading-label="Searching" id="search_btn">
							Search
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="header mrg-bt-20">
	<div class="row group">
		<div class="col span8">
			<h3>
				Search Results
			</h3>
		</div>
		<div class="col span4">
			<div class="form search icon">
				<input type="text" class="field" placeholder="Search" id="search"/>
				<i class="icon fa icon-search"></i>
			</div>
		</div>
	</div>
</div>
<div class="table-group">
	<table class="table table-sort">
		<thead>
			<tr>
				<th width="15%">Last Name <span class="icon fa fa-sort"></span></th>
				<th width="15%">First Name <span class="icon fa fa-sort"></span></th>
				<th width="15%">Mobile Number <span class="icon fa fa-sort"></span></th>
				<th width="10%">Birthday<span class="icon fa fa-sort"></span></th>
				<th width="15%">Available Balance<span class="icon fa fa-sort"></span></th>
				<th width="15%">Wallet ID<span class="icon fa fa-sort"></span></th>
				<th width="15%">Date Created<span class="icon fa fa-sort"></span></th>
			</tr>
		</thead>
		<tbody class="result">
			<!--<tr>
				<td>Moore</td>
				<td>Sam</td>
				<td>0920 281 8192</td>
				<td>09-18-1994</td>
				<td class="nowrap">P 20,000.00</td>
				<td class="nowrap">1234-5678-1234</td>
				<td>08-01-2016</td>
			</tr>
			<tr>
				<td>Lavigne</td>
				<td>Avril</td>
				<td>0917 211 2392</td>
				<td>09-16-1995</td>
				<td class="nowrap">P 25,000.00</td>
				<td class="nowrap">1234-5275-1234</td>
				<td>08-03-2016</td>
			</tr>-->
		</tbody>
		<tfoot class>
			<tr class="no-result center-align">
				<td colspan="7">No results found.</td>
			</tr>
		</tfoot>
		<!-- <tfoot>
			<tr>
				<td colspan="5">
					<div class="empty-data">
						<div class="mrg-bt-20"><i class="icon fa icon-employees"></i></div>
						<div>
							<p class="bold">
								Looks like you haven't uploaded an Employee Registration form yet
							</p>
							<p>
								Details will be available once you upload an Employee Registration form. <a href="#" class="bold orange-color">Learn More</a>
							</p>
						</div>
					</div>
				</td>
			</tr>
		</tfoot> -->
	</table>
</div>