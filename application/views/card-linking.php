<div class="alert-group">
	<div class="alert success hidden">
		<div class="content">
			<p class="title">
				Card Linking Successful
			</p>
			<p class="description">
				<b>1234-4234-5435</b> is linked to <b>1234-1446-0876</b> on 08-07-2016 11:00 AM
			</p>
		</div>
		<i class="icon fa fa-times close"></i>
	</div>
	<div class="alert error hidden">
		<div class="content">
			<p class="title">
				Card Linking Failed
			</p>
			<p class="description">
				Please try again.
			</p>
		</div>
		<i class="icon fa fa-times close"></i>
	</div>
</div>
<div class="breadcrumb">
	<div class="breadcrumb-group">
		<div class="active section">Card Linking</div>
	</div>
</div>
<div class="box">
	<div class="box-title">
		<h3>
			Card Linking
		</h3>
	</div>
	<div class="box-content mrg-top-30">
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="description">
					<div class="form label-set">
						<label for="wallet-id">Wallet ID</label>
						<input type="text" class="field" id="wallet-id" placeholder="Enter wallet ID" data-mirror="#mirrorWalletId">
						<span class="note"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="description">
					<div class="form label-set">
						<label for="card-number">Card Number</label>
						<input type="text" class="field" id="card-number" placeholder="0000 0000 0000" data-mirror="#mirrorCardNumber">
						<span class="note"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="description right-align">
					<div class="form">
						<button class="button orange fat" data-modal="#approve" id="linkSubmit">
							Link
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui modal tiny" id="approve">
	<div class="content">
		<h3 class="center-align">
			Confirm Card Linking
		</h3>
		<div class="mrg-top-20">
			<div class="mrg-top-10">
				<div class="box gray">
					<table class="table basic">
						<tbody>
							<tr>
								<td width="40%" class="bold nowrap">Wallet ID: </td>
								<td class="nowrap"><span id="mirrorWalletId"></span></td>
							</tr>
							<tr>
								<td width="40%" class="bold">Card Number:</td>
								<td class="nowrap"><span id="mirrorCardNumber"></span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="confirm check">
					<div class="confirm-checkbox">
						<div class="ui checkbox">
						  <input type="checkbox" name="confirm" id="confirmLink">
						  <label>&nbsp;</label>
						</div>
					</div>
					<div class="confirm-desc">
						I confirm that the information provided is accurate. <br>
						Clicking <b>CONFIRM</b> will link the card to the wallet.
					</div>
				</div>
				<div class="response center-align">
					<button class="button gray close">
						Cancel
					</button>
					<button class="button orange loading-after" data-loading-label="Processing" id="confirm" disabled>
						Confirm
					</button>
				</div>
			</div>
		</div>
	</div>
</div>