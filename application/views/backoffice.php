<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="True Money">
    <meta name="author" content="True Money">
	
	<?php echo $this->template->meta; ?>

    <title><?php echo ( strlen( $this->template->title ) > 0 ) ? $this->template->title . ' | ' : ""; ?> True Money</title>
	
    <link rel="stylesheet" href="../../assets/css/plugin.css">
    <link rel="stylesheet" href="../../assets/css/backoffice.css">
    <link rel="stylesheet" href="../../assets/font-awesome/css/font-awesome.min.css">
	
	<script type="text/javascript">
		var baseurl     = '<?php echo site_url(); ?>';
	</script>
</head>
<body>
	<nav class="navbar">
        <div class="item brand left-float">
            <img src="<?php echo base_url('../assets/images/logo-truemoney-inverted.png')?>" alt="TrueMoney" class="img-auto-place">
        </div>
        <div class="item toggle-sidebar left-float">
            <span class="icon fa fa-navicon"></span>
        </div>
        <!-- LOGGED IN USER DETAILS -->
		<?php 
			echo $this->template->widget("backoffice/loggedin_user"); 
		?>
        <!-- LOGGED IN USER DETAILS -->
    </nav>
	<!-- MENUS -->
	<?php 
		echo $this->template->widget("backoffice/menus"); 
	?>
	<!-- MENUS -->
	<section class="wrapper prefunding">
        <div class="segment gray">
            <div class="segment-group">
                <div class="segment-content">
					<?php 
						echo $this->template->content;
					?>
				</div>
            </div>
        </div>
    </section>
	<section class="footer">
        <div class="row group">
            <div class="col span6">
                &copy; <a href="#" class="bold orange-color">TrueMoney</a> 2016. All Rights Reserved. <a class="dimmer-trigger orange-color" data-dimmer="#privacy-policy">Privacy</a>
            </div>
            <div class="col span6">
                <div class="right-align">
                     Version 1.0 | Best viewed in Chrome, Firefox and Safari
                </div>
            </div>
        </div>
    </section>
	<!-- PRIVSCY -->
	<?php
		echo $this->template->widget("backoffice/privacy");
	?>
	<!-- PRIVSCY -->
</body>
<script src="../../assets/js/min/plugin.min.js"></script>
<script src="../../assets/js/min/backoffice.min.js"></script>

<?php echo $this->template->javascript; ?>

<?php echo $this->template->stylesheet; ?>
</html>