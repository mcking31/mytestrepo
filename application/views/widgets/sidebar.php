<?php
	$this->load->helper('url');
	
	function format_menu_html( $menus = array(), $html = '', $indention = '---' ){
		foreach( $menus as $key => $values ){
			if( $values['has_access'] ){				
				if( 
					isset( $values['children'] )
					&&
					is_array( $values['children'] ) 
				){
					$html .= '<li>';
					$html .= '<a href="' . base_url( $values['url'] ) . '" >' . $indention . " " . $values['title'] . '</a>';
					$html = format_menu_html( $values['children'], $html, $indention . '---' );
					$html .= '</li>';
				} else 
					$html .= "<li><a href='" . base_url( $values['url'] ) . "'>{$indention} {$values['title']}</a></li>";
			}
		}
		
		return $html;
	}
	
	// $menu_html = format_menu_html( $menus, '', '' );
	
	$menu_html = "
		<li>
			<a href='" . base_url('maintenance') . "'>Lookup Tables</a>
		</li>
	";
?>

<div id="sidebar-wrapper">
	<ul class="sidebar-nav">
		<li class="sidebar-brand">
			<a href="#">
				TRUEMONEY
            </a>
		</li>
        <?php echo $menu_html; ?>
	</ul>
</div>