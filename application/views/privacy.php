<body class="iframe-content">
        <div class="site-brand-container">
            <div class="site-branding">
                <a href="#" class="custom-logo-link">
                    <img src="../../assets/images/logo-truemoney.png" alt="TrueMoney" />
                </a>
                <p class="title">
                    Privacy Policy
                </p>
            </div>
        </div>
        <div class="privacy-group">
            <div class="privacy-content">
                <p>
                    We respect the privacy of everyone who visits the website and provides his/her personal data.  As a result, we wish to inform you regarding the way we shall process and use your personal data.  Please read this privacy policy so that you understand our approach towards the processing of your personal data.
                </p>
                <p>
                    By submitting your personal data to us, you have explicitly consented to the processing of your personal data by TrueMoney as stated in the privacy policy.
                </p>
                <p>
                    Please be informed that TrueMoney will only process the personal data you have supplied for as long as is necessary to achieve the purposes outlined in this Privacy Policy and in line with its data retention guidelines.
                </p>
                <p>
                    By continuing to use TrueMoney products and services, you signify that you have read, understood, and consented to the collection and use of your Customer Data, particularly your Personal Information, in accordance with this Privacy Policy
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Collection of Customer Data
                </h3>
                <p>
                    As part of our continuing relationship with you, we collect your Customer Data, as may be applicable. Customer Data are either Personal Information or Non-Personal Information:
                </p>
                <p>
                    <ol class="lower-roman">
                        <li>
                            Personal Information is any information from which the identity of an individual can be reasonably and directly ascertained, or when put together with other information would directly and certainly identify an individual, such as name, gender, date of birth, address, telephone/mobile number, email address, proof of identification, etc. 
                        </li>
                        <li>
                            Non-Personal Information is any information that does not identify you individually, and includes statistical and analytical data, and anonymous and aggregate reports.
                        </li>
                    </ol>
                </p>
                <p>
                    We may also request you to update your Personal Information from time to time. Should you be unable to supply us with the required Personal Information, we may be unable to provide you with your requested products and services, updates on TrueMoney�s latest offerings, and you may be unable to participate in our events, promotions or other activities.
                </p>
                <p>
                    When you use our website and mobile app and electronically communicate with us, depending on your settings, we may use cookies, web beacons, small data text files or similar technologies to identify your device and record your preferences, with your consent.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Use of Customer Data
                </h3>
                <p>
                    Without limiting the generality of the foregoing, we use Customer Data to, among others:
                </p>
                <p>
                    <ol class="lower-roman">
                        <li>
                            Provide you with your subscribed products and services, including customer support; 
                        </li>
                        <li>
                            Enhance your customer experience and determine tailored content to meet your preferences and needs; 
                        </li>
                        <li>
                            Communicate relevant services and/or advisories to you;
                        </li>
                        <li>
                            Abide by any safety, security, public service or legal requirements and processes; and 
                        </li>
                        <li>
                            Process information for statistical, analytical, and research purposes.
                        </li>
                    </ol>
                </p>
                <p>
                    We use your Personal Information to the extent necessary to comply with the requirements of the law and legal process, such as a court order; to comply with a legal obligation; or to prevent imminent harm to public security, safety or order.
                </p>
                <p>
                    We use your Non-Personal Information for statistical, analytical, and research purposes to create anonymous and aggregate reports.
                </p>
                <p>
                    When required by our Privacy Policy and the law and before we use or process your Customer Data for any other purpose, we will ask for your consent.
                </p>
                <p>
                    You may avail of our broadcast messages relevant to you through written correspondence, text messaging, internet, or other similar means of communication. You may also change your mind anytime and stop receiving them.
                </p>
                <p>
                    We outsource or contract the processing of Customer Data to third parties, such as but not limited to, vendors, service providers, partners or other telecommunications operators, to fulfill any of the above purposes. They are only authorized to use Customer Data for such contracted purposes. They may have access to Customer Data for a limited time under reasonable contractual and technical safeguards to limit their use of such information. We require them to protect Customer Data consistent with our Privacy Policy.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Protection of Customer Data
                </h3>
                <p>
                    We respect your privacy. We take paramount care in protecting your Customer Data. As such, we secure and protect your Customer Data with proper safeguards to ensure confidentiality and privacy; prevent loss, theft, or use for unauthorized purposes; and comply with the requirements of the law.
                </p>
                <p>
                    As communications over the internet may not be secure, we make reasonable and appropriate security arrangements and measures that use a variety of physical, electronic, and procedural safeguards to protect Customer Data. For example, we protect and keep your information submitted through our website safe using a secured server behind a firewall, encryption, and other appropriate security controls. For data encryption, we are using Advanced Encryption Standard (AES) for the data that is passed to our system. We regularly review our information collection, storage, and processing practices, including physical security measures, to guard against unauthorized access to our system and unauthorized alteration, disclosure, or destruction of information we hold.
                </p>
                <p>
                    We only permit your Customer Data to be accessed or processed by our authorized personnel who hold such information under strict confidentiality. We restrict access to information to TrueMoney employees, contractors, and agents who need to know such information in order to process it for us, who are subject to strict contractual and technical safeguards and are accountable if they fail to meet these obligations.
                </p>
                <p>
                    Furthermore, we only give you or your authorized representative access to your Personal Information. We do not sell your Personal Information to anyone for any purpose. We also do not use nor share your Personal Information with content and/or information providers without your prior request or consent. Personal Information will only be disclosed to third parties in accordance with this Privacy Policy.
                </p>
                <p>
                    We keep our records as accurate as possible. If your Personal Information is wrong, we give you ways to update it. Once you have registered as our customer, you may access your account details and correct your Personal Information by contacting TrueMoney Customer Loyalty (+632 718 9999) or your account manager, as may be applicable; or by visiting any TrueMoney Agent Store or our website at <a href="https://www.truemoneyph.com" target="_blank" class="orange-link-undeline-normal">https://www.truemoneyph.com.</a>
                </p>
                <p>
                    We keep your Personal Information in our business records, as may be applicable, while you are a customer, or as long as it is necessary to fulfill the purpose for which it was collected, or while it is needed by us for business, tax, or legal purposes. When disposing of your Personal Information, we take reasonable measures to ensure that it is done properly and is not accessible to the public.
                </p>
                <p>
                    We are not responsible for information, content, application, product or service that we do not provide. But because we care for you and protect you, we take measures to fight spam, fraud or any unauthorized messages that traverse our network.
                </p>
                <p>
                    Photo or video voyeurism, including the publication or broadcast or transmission through our network of any sexual act or any similar activity without the consent of the person involved and under circumstances in which the person has a reasonable expectation of privacy, is prohibited and unlawful with corresponding penalties under the law.
                </p>
                <p>
                    Child pornography is also prohibited and punishable by law and our network shall not be used in any manner for the storage, transmission, or dissemination of materials containing child pornography. We will report any instances of such activity that we become aware of to the proper authorities as required by law.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Special Notice - if you are under 13 years old
                </h3>
                <p>
                    Our website is not aimed at children under 13 years old and we will not collect, use, provide or process in any other form any personal information of children under the age of 13 deliberately. We therefore also ask you, if you are under 13 years old, please do not send us your personal information (for example, your name, address and email address).
                </p>
                <p>
                    If you are under 13 years old and you nevertheless wish to ask a question or use this website in anyway which requires you to submit your personal information, please get your parent or guardian to do so on your behalf. 
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Transfer of Your Data Abroad
                </h3>
                <p>
                    By voluntarily providing us with your Personal Data, you are consenting to our use of it in accordance with this Privacy Policy. Due to the nature of the Internet if you are visiting this Site from a country other than any of the Jurisdictions, your communications will inevitably result in the transfer of information across international boundaries. If you provide Personal Data to this Site, you acknowledge and agree that such Personal Data may be transferred from your current location to the offices and servers of True and the affiliates, agents and service providers referred to herein located in other countries. In the event that data is transferred to a country whose laws do not provide equivalent data protection, TrueMoney shall ensure adequate protection of such Personal Data in accordance with this Privacy Policy.
                </p>
                <p>
                    By visiting this web site and providing Personal Data to this Site, you consent to these transfers.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Interaction between you and us
                </h3>
                <p>
                    We are interested in your views, and we value feedback from our clients and visitors, we therefore have set up notice boards, newsgroups, feedback, email, forum facilities and/ or chat rooms. If at any time this website offers any chat rooms, notice board facilities, newsgroups etc. we may collect the personal information that you disclose.
                </p>
                <p>
                    Such information will be used in accordance with this privacy policy. However, we can of course not control and be responsible for other parties' use of the personal information which you make available to them through this website. We encourage you to be careful about what personal information you disclose in this way.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Keeping our records accurate
                </h3>
                <p>
                    We aim to keep our information about you as accurate as possible. If you would like to review, change or delete the details you have supplied us with, please contact us as set out below.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Security of your personal data
                </h3>
                <p>
                    As we value your personal information, we will ensure an adequate level of protection. We have therefore implemented technology and policies with the objective of protecting your privacy from unauthorized access and improper use and will update these measures as new technology becomes available, as appropriate.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Use of your personal information submitted to other web sites
                </h3>
                <p>
                    On our website, we may have links to other websites or you are referred to our website through a link from another website. As you can imagine, we cannot be responsible for the privacy policies and practices of other websites.  Such content is subject to their terms of use and any additional guidelines and privacy information provided in relation to that use on their website. 
                </p>
                <p>
                    We recommend that you check the policy of each website you visit to better understand your rights and obligations especially when you are submitting any type of content on those third party website. Please contact the owner or operator of such website if you have any concerns or questions.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Cookies
                </h3>
                <p>
                    We may use cookies to deliver content specific to your interests or for other purposes. Promotions or advertisements displayed on our site may contain cookies. We do not have access to or control over information collected by outside advertisers on our site.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Comments
                </h3>
                <p>
                    The Site may include interactive sections such as commenting areas where visitors to the site can post comments. Please remember that any information that is disclosed in these areas becomes public information and you should exercise caution when deciding to disclose your personal information. We are not responsible for any information published in the comments section of this website.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Updates and Changes
                </h3>
                <p>
                    We reserve the right, at any time, to add to, change, update, or modify this Policy, simply by posting such change, update, or modification on the Site and without any other notice to you. Any such change, update, or modification will be effective immediately upon posting on the Site. It is your responsibility to review this Policy from time to time to ensure that you continue to agree with all of its terms.
                </p>
            </div>
            <div class="privacy-content">
                <h3>
                    Contact Us
                </h3>
                <p>
                    For any questions or concerns about our privacy policy, you may contact our Privacy Officer at <b>(632) 718 9999</b> or via e-mail <a href="mailto:privacy@truemoneyph.com" class="orange-link-undeline-normal">privacy@truemoneyph.com</a>
                </p>
            </div>
        </div>
    </body>