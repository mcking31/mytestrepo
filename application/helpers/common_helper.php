<?php 

if (!function_exists('debug')){
    function debug($string = ''){
		if( !is_array( $string ) ):
			echo $string;
		else:
			echo '<pre>';
			print_r($string);
			echo '</pre>';
		endif;
    }
}

if (!function_exists('get_rest_auth')){
    function get_rest_auth(){
		$ci =& get_instance();
		
		$response = [
			'user' => $ci->input->server('PHP_AUTH_USER'),
			'pass' => $ci->input->server('PHP_AUTH_PW')
		];
		
		return $response;
	}
}		
if (!function_exists('get_rest_headers')){
    function get_rest_headers(){
		$ci =& get_instance();
		$response = $ci->input->request_headers();
		$ignore_headers = array('connection', 'content-length', 'user-agent', 'origin', 'authorization', 'content-type', 'accept', 'accept-encoding', 'accept-language', 'cookie');
		
		foreach ($response as $header => $value){
			switch( strtolower( $header ) ){
				case 'host':
					$response['client_host'] = $response[ $header ];
					unset( $response[ $header ] );
				break;
				default:
					if( in_array(strtolower($header), $ignore_headers) )
						unset( $response[$header] );
				break;
			}
		}
		
		return $response;
    }
}
?>