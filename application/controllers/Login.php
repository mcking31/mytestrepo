<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		
		$this->load->library('api');
		$this->load->helper('form');
	}
	
	public function index(){
		if( isset($_SESSION["PartnerUserId"]) )
			redirect('dashboard','refresh');
			
		$data = [];
		
		$this->template->build_template(
			'Login',
			array(
				array(
					'view' => 'login',
					'data' => $data
				)
			),
			array(
				'../assets/js/min/plugin.min.js',
				'assets/module_js/login.js'
			),
			array(
				'../assets/css/plugin.css',
				'../assets/css/backoffice.css',
				'../assets/font-awesome/css/font-awesome.min.css',
			),
			array(),
			'blank'
		);
	}
	
	public function verify(){
		$data['response'] = FALSE;
		
		try{			
			// CORE
			$this->load->library('api');
			$ucore = $this->api->login( ['Username' => $this->input->post('username'), 'Password' => $this->input->post('password')] );
			
			if( $ucore['Result'] == 0 ):
				$data = [
					'response' => TRUE,
					'message' => $ucore['Message']
				];
				
				$_user = array(
					'adminUsersId' => $ucore['data']['adminUsersId'],
					'group_id' => $ucore['data']['Role'],
					'data' => $ucore['data']
				);
				
				$this->session->set_userdata( $_user );
			else:
				$data = [
					'response' => FALSE,
					'message' => $ucore['Message'],
					'trn' => $ucore['ReferenceID']
				];
			endif;
			// END CORE
			
			
		} catch( Exception $e ) {
			$data['message'] = $e->getMessage();
		}
		
		header( 'Content-Type: application/x-json' );
		echo json_encode( $data );
	}
	
	// public function verify()
	// {
		// $username = $this->input->post('username');
		// $password = $this->input->post('password');

		// // CORE
		// $this->load->library('api');
		// $ucore = $this->api->login( ['Username' => $username, 'Password' => $password] );
	
		// if( $ucore['Result'] == 0 ):
			// //$a = json_decode( $ucore['data']['partner_user_role'], TRUE );
			// $users[] = [
				// 'username' => $username,
				// 'user_role' => strtolower( $ucore['data']['partner_user_role']['Name'] )
			// ];
		// endif;
		// // END CORE

		// $success = FALSE;

		// $sess_user_role = null;

		// foreach( $users as $row ):

			// if( $row['username'] == $username ){

				// $sess_user_role = $row['user_role'];
				// // die('sdfs' . $sess_user_role);
				// $success = TRUE;
				// break;
			// }

		// endforeach;

		// // echo $username . ' ' . $password . ' ';
		// // echo $success . '<--';
		// // exit;

		// if( $success ){
			// // die('sdfsd');
			// // $sess_array = array();

			// $sess_array = array(
	         // 'username' => $username
	         // ,'user_role' => $sess_user_role
	       // );

			// // die('sdfsdf');
	       // // $this->session->set_userdata('logged_in', $sess_array);
	       // $_SESSION["logged_in"] = $sess_array;
		   // $_SESSION['user_data'] = $ucore['data'] ?? [];
		   // $_SESSION['user_data']['partner_user_role'] = $ucore['data']['partner_user_role'];//json_decode( $ucore['data']['partner_user_role'], TRUE );
		
	       // redirect( base_url('dashboard') , 'refresh');
		// }else{
			// redirect(base_url('login'), 'refresh');
		// }
	// }
	
	public function destroy(){
		$this->session->sess_destroy();
		debug( $_SESSION );
		echo "sadas";
		die();
		// redirect( base_url('login') );
	}
}