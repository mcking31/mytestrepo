<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Truemoney extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function privacy(){
		$this->template->build_template(
			'Privacy Policy',
			array(
				array(
					'view' => 'privacy'
				)
			),
			array(
				
			),
			array(
				'../../assets/css/plugin.css',
				'../../assets/css/main.css',
				'../../assets/font-awesome/css/font-awesome.min.css',
			),
			array(),
			'blank'
		);
	}
}