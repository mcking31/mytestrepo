<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$_a = $this->common->get_session('data');
		$data = [
			'data' => $_a
		];
		
		$this->template->build_template(
			'Dashboard',
			array(
				array(
					'view' => 'dashboard',
					'data' => $data
				)
			),
			array(
				// JS
			),
			array(),
			array(),
			'backoffice'
		);
	}
}