<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Reports extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$data = [];
		
		$this->template->build_template(
			'Reports',
			array(
				array(
					'view' => 'reports',
					'data' => $data
				)
			),
			array(
				'assets/reports.js'
			),
			array(),
			array(),
			'backoffice'
		);
	}
}