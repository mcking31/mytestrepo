<?php

class Loggedin_user extends Widget {

    public function display($data) {
		$this->load->library('common');
		
		$_a = $this->common->get_session('data');
		
		$data = [
			'firstname' => $_a['FirstName'],
			'lastname' => $_a['LastName']
		];
		
		$this->view('widgets/backoffice/loggedin_user', $data);
    }
	
}